package com.markel.transportmanager.application.schedulers;

import com.markel.transportmanager.application.applications.abstractions.TransportApplication;
import com.markel.transportmanager.domain.exceptions.DomainException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class UpdateTransportsScheduler {

    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateTransportsScheduler.class);

    private final TransportApplication transportApplication;

    @Autowired
    public UpdateTransportsScheduler(TransportApplication transportApplication) {
        this.transportApplication = transportApplication;
    }

    @Scheduled(fixedDelayString = "${api.scheduler.time}", initialDelay = 2000)
    public void updateTransports() {
        LOGGER.info("EXECUTING REFRESH TRANSPORTS SCHEDULER");
        try {
            this.transportApplication.refreshTransports();
        } catch (DomainException e) {
            LOGGER.error("SCHEDULED TASK FAILED");
            LOGGER.error(e.getMessage());
        }
    }
}
