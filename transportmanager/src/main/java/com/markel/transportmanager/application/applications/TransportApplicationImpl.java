package com.markel.transportmanager.application.applications;

import com.markel.transportmanager.application.applications.abstractions.TransportApplication;
import com.markel.transportmanager.application.applications.abstractions.model.TransportResponse;
import com.markel.transportmanager.application.applications.abstractions.model.mappers.TransportResponseMapper;
import com.markel.transportmanager.domain.exceptions.DomainException;
import com.markel.transportmanager.domain.services.abstractions.TransportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class TransportApplicationImpl implements TransportApplication {

    private final TransportService transportService;

    @Value("${api.city}")
    private String city;

    @Value("#{${api.scheduler.params}}")
    private Map<String, String> filters;

    @Autowired
    public TransportApplicationImpl(TransportService transportService) {
        this.transportService = transportService;
    }

    @Override
    public void refreshTransports() throws DomainException {
        this.transportService.refreshTransports(city, filters);
    }

    @Override
    public List<TransportResponse> getTransports() throws DomainException {
        var transports = this.transportService.getTransports();
        return TransportResponseMapper.INSTANCE.transportsToTransportResponses(transports);
    }
}
