package com.markel.transportmanager.application.applications.abstractions.model.mappers;

import com.markel.transportmanager.application.applications.abstractions.model.TransportResponse;
import com.markel.transportmanager.domain.model.Transport;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface TransportResponseMapper {

    TransportResponseMapper INSTANCE = Mappers.getMapper(TransportResponseMapper.class);

    /* DOMAIN to APPLICATION RESPONSE */
    List<TransportResponse> transportsToTransportResponses(List<Transport> transports);

    @Mapping(target = "x", source = "transport.location.x")
    @Mapping(target = "y", source = "transport.location.y")
    TransportResponse transportToTransportResponse(Transport transport);
}