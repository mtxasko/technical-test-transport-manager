package com.markel.transportmanager.application.applications.abstractions.model;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.Map;

@Getter
@Setter
public class TransportResponse {

    private String id;
    private String name;
    private String city;
    private Date lastUpdated;
    private String x;
    private String y;
    private String companyZoneId;
    @JsonAnyGetter
    @Getter(AccessLevel.NONE)
    private Map<String, Object> properties;

}
