package com.markel.transportmanager.application.controllers;

import com.markel.transportmanager.application.applications.abstractions.TransportApplication;
import com.markel.transportmanager.application.applications.abstractions.model.TransportResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TransportController {

    final TransportApplication transportApplication;

    @Autowired
    public TransportController(TransportApplication transportApplication) {
        this.transportApplication = transportApplication;
    }

    @GetMapping
    public List<TransportResponse> getAllTransports() throws Exception {
        return this.transportApplication.getTransports();
    }

    @PostMapping
    public void refreshTransports() throws Exception {
        this.transportApplication.refreshTransports();
    }
}
