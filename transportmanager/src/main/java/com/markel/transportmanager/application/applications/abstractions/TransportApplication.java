package com.markel.transportmanager.application.applications.abstractions;

import com.markel.transportmanager.application.applications.abstractions.model.TransportResponse;
import com.markel.transportmanager.domain.exceptions.DomainException;

import java.util.List;

public interface TransportApplication {

    List<TransportResponse> getTransports() throws DomainException;

    void refreshTransports() throws DomainException;
}
