package com.markel.transportmanager.infrastructure.utils;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@Component
public class MarshallingUtils {

    private final ObjectMapper mapper;

    public MarshallingUtils() {
        this.mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    public <T> List<T> unmarshallToList(String data, Class<T[]> targetClass) throws IOException {
        T[] transformedData = this.mapper.readValue(data, targetClass);
        return Arrays.asList(transformedData);
    }

    public String serializeObject(Object data) throws IOException {
        return this.mapper.writeValueAsString(data);
    }

}
