package com.markel.transportmanager.infrastructure.notifications.model.enums;

public enum TransportsUpdateTypeEnum {
    MODIFIED,
    DELETED
}
