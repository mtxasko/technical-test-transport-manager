package com.markel.transportmanager.infrastructure.persistence.adapters;

import com.amazonaws.SdkClientException;
import com.markel.transportmanager.domain.exceptions.InfrastructureException;
import com.markel.transportmanager.domain.model.Transport;
import com.markel.transportmanager.domain.ports.TransportPersistence;
import com.markel.transportmanager.infrastructure.persistence.model.mappers.TransportEntityMapper;
import com.markel.transportmanager.infrastructure.persistence.repositories.TransportRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class TransportPersistenceImpl implements TransportPersistence {

    private static final Logger LOGGER = LoggerFactory.getLogger(TransportPersistenceImpl.class);

    private final TransportRepository transportRepository;

    @Autowired
    public TransportPersistenceImpl(TransportRepository transportRepository) {
        this.transportRepository = transportRepository;
    }

    @Override
    public List<Transport> getAll() throws InfrastructureException {
        try {
            var repositoryTransports = this.transportRepository.findAll();
            return TransportEntityMapper.INSTANCE
                    .transportEntitiesToTransports(repositoryTransports);
        } catch (SdkClientException e) {
            throw new InfrastructureException(e.getMessage());
        }
    }

    @Override
    public List<Transport> getAllByCity(String city) throws InfrastructureException {
        try {
            var repositoryTransports = this.transportRepository.findByCity(city);
            return TransportEntityMapper.INSTANCE
                    .transportEntitiesToTransports(repositoryTransports);
        } catch (SdkClientException e) {
            throw new InfrastructureException(e.getMessage());
        }
    }

    @Override
    public List<Transport> updateTransports(List<Transport> transports) throws InfrastructureException {
        LOGGER.info("Updating transports in db:");
        LOGGER.info(transports.stream().map(Transport::getId).collect(Collectors.toList()).toString());
        try {
            var repositoryTransports = TransportEntityMapper.INSTANCE.transportsToTransportEntities(transports);
            var result = this.transportRepository.saveAll(repositoryTransports);
            return TransportEntityMapper.INSTANCE.transportEntitiesToTransports(result);
        } catch (SdkClientException e) {
            throw new InfrastructureException(e.getMessage());
        }
    }

    @Override
    public void deleteTransportsById(List<String> transportIds) throws InfrastructureException {
        if (transportIds.isEmpty()) {
            return;
        }

        try {
            LOGGER.info("Deleting transports from db:");
            LOGGER.info(transportIds.toString());
            this.transportRepository.deleteByIdIn(transportIds);
        } catch (SdkClientException e) {
            throw new InfrastructureException(e.getMessage());
        }
    }
}
