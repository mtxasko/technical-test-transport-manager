package com.markel.transportmanager.infrastructure.http.adapters;

import com.markel.transportmanager.domain.exceptions.InfrastructureException;
import com.markel.transportmanager.domain.model.Transport;
import com.markel.transportmanager.domain.ports.TransportProvider;
import com.markel.transportmanager.infrastructure.http.model.TransportApiResponse;
import com.markel.transportmanager.infrastructure.http.model.mappers.TransportApiResponseMapper;
import com.markel.transportmanager.infrastructure.http.services.HttpProviderImpl;
import org.apache.commons.text.StringSubstitutor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Component
public class TransportProviderImpl implements TransportProvider {

    private final HttpProviderImpl httpProvider;
    private final String CITIES_ENDPOINT = "${city}/resources";

    @Value("${api.baseUri}")
    private String apiUri;

    @Autowired
    public TransportProviderImpl(HttpProviderImpl httpProvider) {
        this.httpProvider = httpProvider;
    }

    @Override
    public List<Transport> getLatestTransports(String city, Map<String, String> filters) throws InfrastructureException {
        var transportResponses = this.httpProvider.get(
                this.constructBaseUri(city), filters, TransportApiResponse[].class);
        return TransportApiResponseMapper.INSTANCE
                .transportResponsesToTransports(transportResponses, city, new Date());
    }

    private String constructBaseUri(String city) {
        var transportUriWithCity = StringSubstitutor.replace(this.CITIES_ENDPOINT, Map.of("city", city));
        return this.apiUri.concat(transportUriWithCity);
    }

}
