package com.markel.transportmanager.infrastructure.notifications.adapters;

import com.markel.transportmanager.domain.exceptions.InfrastructureException;
import com.markel.transportmanager.domain.model.Transport;
import com.markel.transportmanager.domain.ports.TransportNotifier;
import com.markel.transportmanager.infrastructure.notifications.model.TransportsUpdatedMessage;
import com.markel.transportmanager.infrastructure.notifications.model.enums.TransportsUpdateTypeEnum;
import com.markel.transportmanager.infrastructure.notifications.services.NotificationProviderImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class TransportNotifierImpl implements TransportNotifier {

    @Value("${kafka.topics.transports}")
    private String transportsTopic;

    private final NotificationProviderImpl notificationProvider;

    @Autowired
    public TransportNotifierImpl(NotificationProviderImpl notificationProvider) {
        this.notificationProvider = notificationProvider;
    }

    @Override
    public void notifyCreatedUpdatedTransports(List<Transport> transports) throws InfrastructureException {
        if (!transports.isEmpty()) {
            var message = new TransportsUpdatedMessage<>(TransportsUpdateTypeEnum.MODIFIED, transports);
            this.notificationProvider.send(transportsTopic, message);
        }
    }

    @Override
    public void notifyDeletedTransports(List<String> transportsIds) throws InfrastructureException {
        if (!transportsIds.isEmpty()) {
            var message = new TransportsUpdatedMessage<>(TransportsUpdateTypeEnum.DELETED, transportsIds);
            this.notificationProvider.send(transportsTopic, message);
        }
    }

}
