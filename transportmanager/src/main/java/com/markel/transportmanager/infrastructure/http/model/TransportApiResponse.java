package com.markel.transportmanager.infrastructure.http.model;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
public class TransportApiResponse {
    private String id;
    private String name;
    private String city;
    private String x;
    private String y;
    private Boolean realTimeData;
    private String companyZoneId;
    private Map<String, Object> properties = new HashMap<>();

    @JsonAnySetter
    public void add(String key, Object value) {
        properties.put(key, value);
    }
}
