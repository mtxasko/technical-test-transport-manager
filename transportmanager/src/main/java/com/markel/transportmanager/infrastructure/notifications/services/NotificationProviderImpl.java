package com.markel.transportmanager.infrastructure.notifications.services;

import com.markel.transportmanager.domain.exceptions.InfrastructureException;
import com.markel.transportmanager.infrastructure.notifications.services.abstractions.NotificationProvider;
import com.markel.transportmanager.infrastructure.utils.MarshallingUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.KafkaException;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class NotificationProviderImpl implements NotificationProvider {

    private static final Logger LOGGER = LoggerFactory.getLogger(NotificationProviderImpl.class);

    private final KafkaTemplate<String, String> kafkaTemplate;
    private final MarshallingUtils marshallingUtils;

    @Autowired
    public NotificationProviderImpl(KafkaTemplate<String, String> kafkaTemplate, MarshallingUtils marshallingUtils) {
        this.kafkaTemplate = kafkaTemplate;
        this.marshallingUtils = marshallingUtils;
    }

    @Override
    public void send(String topic, Object data) throws InfrastructureException {
        try {
            var serializedData = this.marshallingUtils.serializeObject(data);
            LOGGER.info("Sending message to topic ".concat(topic).concat(": ").concat(serializedData));
            this.kafkaTemplate.send(topic, serializedData);
        } catch (KafkaException | IOException e) {
            throw new InfrastructureException(e.getMessage());
        }
    }
}
