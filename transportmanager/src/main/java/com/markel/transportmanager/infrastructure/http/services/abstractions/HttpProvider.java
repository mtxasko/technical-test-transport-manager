package com.markel.transportmanager.infrastructure.http.services.abstractions;

import com.markel.transportmanager.domain.exceptions.InfrastructureException;

import java.util.List;
import java.util.Map;

public interface HttpProvider {
    <T> List<T> get(String baseUri, Map<String, String> params, Class<T[]> targetClass) throws InfrastructureException;
}
