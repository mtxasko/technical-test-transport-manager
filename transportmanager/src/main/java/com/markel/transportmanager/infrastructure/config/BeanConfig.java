package com.markel.transportmanager.infrastructure.config;

import com.markel.transportmanager.domain.ports.TransportNotifier;
import com.markel.transportmanager.domain.ports.TransportPersistence;
import com.markel.transportmanager.domain.ports.TransportProvider;
import com.markel.transportmanager.domain.services.TransportServiceImpl;
import com.markel.transportmanager.domain.services.abstractions.TransportService;
import com.markel.transportmanager.infrastructure.http.adapters.TransportProviderImpl;
import com.markel.transportmanager.infrastructure.http.adapters.TransportProviderSimulatorImpl;
import com.markel.transportmanager.infrastructure.http.services.HttpProviderImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfig {

    @Value("${api.simulator}")
    private boolean apiSimulator;

    @Bean
    TransportService transportService(TransportProvider transportProvider, TransportNotifier transportNotifier,
                                      TransportPersistence transportPersistence) {
        return new TransportServiceImpl(transportProvider, transportNotifier, transportPersistence);
    }

    @Bean
    TransportProvider transportProvider(HttpProviderImpl httpProvider) {
        if (apiSimulator) {
            return new TransportProviderSimulatorImpl();
        } else {
            return new TransportProviderImpl(httpProvider);
        }
    }

}
