package com.markel.transportmanager.infrastructure.notifications.model;

import com.markel.transportmanager.infrastructure.notifications.model.enums.TransportsUpdateTypeEnum;
import lombok.Getter;

import java.util.List;

@Getter
public class TransportsUpdatedMessage<T> {
    TransportsUpdateTypeEnum updateType;
    List<T> data;

    public TransportsUpdatedMessage(TransportsUpdateTypeEnum updateType, List<T> data) {
        this.updateType = updateType;
        this.data = data;
    }
}
