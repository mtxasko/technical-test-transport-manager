package com.markel.transportmanager.infrastructure.http.adapters;

import com.markel.transportmanager.domain.model.Transport;
import com.markel.transportmanager.domain.ports.TransportProvider;
import org.jeasy.random.EasyRandom;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

@Component
public class TransportProviderSimulatorImpl implements TransportProvider {

    @Override
    public List<Transport> getLatestTransports(String city, Map<String, String> filters) {
        EasyRandom generator = new EasyRandom();
        var numberOfTransports = new Random().nextInt(9) + 1;

        return generator
                .objects(Transport.class, numberOfTransports)
                .peek(t -> {
                    t.setCity(city);
                    t.setProperties(this.generateRandomProperties());
                })
                .collect(Collectors.toList());
    }

    private Map<String, Object> generateRandomProperties() {
        EasyRandom generator = new EasyRandom();
        var result = new HashMap<String, Object>();
        var numberOfProperties = new Random().nextInt(5) + 1;
        for (int i = 0; i < numberOfProperties; i++) {
            result.put(generator.nextObject(String.class), generator.nextObject(String.class));
        }
        return result;
    }

}
