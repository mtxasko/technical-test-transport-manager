package com.markel.transportmanager.infrastructure.notifications.services.abstractions;

import com.markel.transportmanager.domain.exceptions.InfrastructureException;

public interface NotificationProvider {
    void send(String topic, Object data) throws InfrastructureException;
}
