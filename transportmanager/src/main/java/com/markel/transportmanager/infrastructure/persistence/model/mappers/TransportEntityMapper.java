package com.markel.transportmanager.infrastructure.persistence.model.mappers;

import com.markel.transportmanager.domain.model.Transport;
import com.markel.transportmanager.infrastructure.persistence.model.TransportEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface TransportEntityMapper {

    TransportEntityMapper INSTANCE = Mappers.getMapper( TransportEntityMapper.class );

    /* ENTITY TO DOMAIN */
    @Mapping(target = "location", source = "locationEntity")
    Transport transportEntityToTransport(TransportEntity transportEntity);
    List<Transport> transportEntitiesToTransports(Iterable<TransportEntity> transportEntities);

    /* DOMAIN TO ENTITY */
    @Mapping(target = "locationEntity", source = "location")
    TransportEntity transportToTransportEntity(Transport transport);
    Iterable<TransportEntity> transportsToTransportEntities(Iterable<Transport> transportEntities);
}