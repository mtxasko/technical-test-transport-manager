package com.markel.transportmanager.infrastructure.http.services;

import com.markel.transportmanager.domain.exceptions.InfrastructureException;
import com.markel.transportmanager.infrastructure.http.services.abstractions.HttpProvider;
import com.markel.transportmanager.infrastructure.utils.MarshallingUtils;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

@Component
public class HttpProviderImpl implements HttpProvider {

    private static final Logger LOGGER = LoggerFactory.getLogger(HttpProviderImpl.class);

    private final CloseableHttpClient client;
    private final MarshallingUtils marshallingUtils;

    @Autowired
    public HttpProviderImpl(CloseableHttpClient client, MarshallingUtils marshallingUtils) {
        this.client = client;
        this.marshallingUtils = marshallingUtils;
    }

    @Override
    public <T> List<T> get(String baseUri, Map<String, String> params, Class<T[]> targetClass) throws InfrastructureException {
        try {
            var uri = this.getUri(baseUri, params);
            var httpRequest = this.getHttpRequest(uri, ContentType.APPLICATION_JSON);
            return this.executeRequestAndGetResponseData(httpRequest, targetClass);
        } catch (URISyntaxException | IOException e) {
            throw new InfrastructureException(e.getMessage());
        }
    }

    private URI getUri(String baseUri, Map<String, String> params) throws URISyntaxException {
        var builder = new URIBuilder(baseUri);
        if (params != null) {
            for (var paramEntry : params.entrySet()) {
                builder.addParameter(paramEntry.getKey(), paramEntry.getValue());
            }
        }
        return builder.build();
    }

    private HttpRequestBase getHttpRequest(URI uri, ContentType contentType) {
        var httpRequest = new HttpGet(uri);
        httpRequest.addHeader("accept", String.valueOf(contentType));
        return httpRequest;
    }

    private <T> List<T> executeRequestAndGetResponseData(HttpRequestBase httpRequest,
                                                         Class<T[]> targetClass) throws IOException {
        LOGGER.info("Calling {}", httpRequest.getURI());
        var response = this.client.execute(httpRequest);
        var stringResponse = EntityUtils.toString(response.getEntity());
        return marshallingUtils.unmarshallToList(stringResponse, targetClass);
    }

}
