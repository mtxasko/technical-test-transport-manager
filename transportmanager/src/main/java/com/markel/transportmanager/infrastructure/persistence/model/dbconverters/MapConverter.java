package com.markel.transportmanager.infrastructure.persistence.model.dbconverters;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class MapConverter implements DynamoDBTypeConverter<String, Map<String, Object>> {

    private static final Logger LOGGER = LoggerFactory.getLogger(MapConverter.class);

    private final ObjectMapper objectMapper;

    public MapConverter() {
        this.objectMapper = new ObjectMapper();
    }

    @Override
    public String convert(Map<String, Object> stringObjectMap) {
        try {
            return this.objectMapper.writeValueAsString(stringObjectMap);
        } catch (JsonProcessingException e) {
            LOGGER.error(e.getMessage());
            return null;
        }
    }

    @Override
    public Map<String, Object> unconvert(String s) {
        try {
            return this.objectMapper.readValue(s, new TypeReference<Map<String, Object>>() {
            });
        } catch (JsonProcessingException e) {
            LOGGER.error(e.getMessage());
            return new HashMap<>();
        }
    }

}
