package com.markel.transportmanager.infrastructure.persistence.repositories;

import com.markel.transportmanager.infrastructure.persistence.model.TransportEntity;
import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

@EnableScan
public interface TransportRepository extends CrudRepository<TransportEntity, String> {
    List<TransportEntity> findByCity(String city);

    void deleteByIdIn(List<String> id);
}
