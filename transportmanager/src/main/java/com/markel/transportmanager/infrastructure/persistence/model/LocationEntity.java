package com.markel.transportmanager.infrastructure.persistence.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import lombok.Setter;

@DynamoDBDocument()
@Setter
public class LocationEntity {
    private String x;
    private String y;

    @DynamoDBAttribute
    public String getX() {
        return x;
    }

    @DynamoDBAttribute
    public String getY() {
        return y;
    }
}
