package com.markel.transportmanager.infrastructure.notifications.config;

import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.core.KafkaAdmin;

import java.util.HashMap;
import java.util.Map;

public class TopicsConfig {

    @Value("${kafka.address}")
    private String kafkaAddress;

    @Value("${kafka.topics.transports}")
    private String transportsTopic;

    @Bean
    public KafkaAdmin kafkaAdmin() {
        Map<String, Object> configs = new HashMap<>();
        configs.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaAddress);
        return new KafkaAdmin(configs);
    }

    @Bean
    public NewTopic transportsTopic() {
        return new NewTopic(transportsTopic, 1, (short) 1);
    }

}
