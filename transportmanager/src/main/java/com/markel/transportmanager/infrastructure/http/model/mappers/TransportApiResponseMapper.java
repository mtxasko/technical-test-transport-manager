package com.markel.transportmanager.infrastructure.http.model.mappers;

import com.markel.transportmanager.domain.model.Location;
import com.markel.transportmanager.domain.model.Transport;
import com.markel.transportmanager.infrastructure.http.model.TransportApiResponse;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

import java.util.Date;
import java.util.List;

@Mapper
public interface TransportApiResponseMapper {

    TransportApiResponseMapper INSTANCE = Mappers.getMapper(TransportApiResponseMapper.class);

    /* API RESPONSE TO DOMAIN */
    List<Transport> transportResponsesToTransports(List<TransportApiResponse> transportApiResponses,
                                                   @Context String city,
                                                   @Context Date date);

    @Mapping(target = "location", source = ".", qualifiedByName = "toLocation")
    @Mapping(target = "lastUpdated", ignore = true)
    Transport transportResponseToTransport(TransportApiResponse transportApiResponse, @Context String city,
                                           @Context Date date);

    @Named("toLocation")
    default Location toLocation(TransportApiResponse transportApiResponse) {
        return new Location(transportApiResponse.getX(), transportApiResponse.getY());
    }

    @AfterMapping
    default void setCityAndUpdatedDate(@MappingTarget Transport transport, @Context String city, @Context Date date) {
        transport.setCity(city);
        transport.setLastUpdated(date);
    }
}