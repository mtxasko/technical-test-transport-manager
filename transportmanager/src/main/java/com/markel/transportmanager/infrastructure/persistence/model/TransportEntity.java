package com.markel.transportmanager.infrastructure.persistence.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverted;
import com.markel.transportmanager.infrastructure.persistence.model.dbconverters.MapConverter;
import lombok.Setter;

import java.util.Date;
import java.util.Map;

@DynamoDBTable(tableName = "Transport")
@Setter
public class TransportEntity {
    private String id;
    private String name;
    private String city;
    private Date lastUpdated;
    private LocationEntity locationEntity;
    private Boolean realTimeData;
    private String companyZoneId;
    private Map<String, Object> properties;

    @DynamoDBHashKey(attributeName = "Id")
    public String getId() {
        return id;
    }

    @DynamoDBAttribute(attributeName = "City")
    public String getCity() {
        return city;
    }

    @DynamoDBAttribute(attributeName = "Name")
    public String getName() {
        return name;
    }

    @DynamoDBAttribute(attributeName = "LastUpdated")
    public Date getLastUpdated() {
        return lastUpdated;
    }

    @DynamoDBAttribute(attributeName = "Location")
    public LocationEntity getLocationEntity() {
        return locationEntity;
    }

    @DynamoDBAttribute(attributeName = "RealTimeData")
    public Boolean getRealTimeData() {
        return realTimeData;
    }

    @DynamoDBAttribute(attributeName = "CompanyZoneId")
    public String getCompanyZoneId() {
        return companyZoneId;
    }

    @DynamoDBAttribute(attributeName = "Properties")
    @DynamoDBTypeConverted(converter = MapConverter.class)
    public Map<String, Object> getProperties() {
        return properties;
    }
}
