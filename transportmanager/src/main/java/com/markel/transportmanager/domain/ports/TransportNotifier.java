package com.markel.transportmanager.domain.ports;

import com.markel.transportmanager.domain.exceptions.InfrastructureException;
import com.markel.transportmanager.domain.model.Transport;

import java.util.List;

public interface TransportNotifier {

    void notifyCreatedUpdatedTransports(List<Transport> transports) throws InfrastructureException;

    void notifyDeletedTransports(List<String> transportsIds) throws InfrastructureException;

}
