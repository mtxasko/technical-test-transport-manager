package com.markel.transportmanager.domain.ports;

import com.markel.transportmanager.domain.exceptions.InfrastructureException;
import com.markel.transportmanager.domain.model.Transport;

import java.util.List;

public interface TransportPersistence {
    List<Transport> getAll() throws InfrastructureException;

    List<Transport> getAllByCity(String city) throws InfrastructureException;

    List<Transport> updateTransports(List<Transport> transports) throws InfrastructureException;

    void deleteTransportsById(List<String> transportIds) throws InfrastructureException;
}
