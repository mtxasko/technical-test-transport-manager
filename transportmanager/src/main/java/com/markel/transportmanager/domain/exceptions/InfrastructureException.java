package com.markel.transportmanager.domain.exceptions;

public class InfrastructureException extends Exception {

    private final String message;

    public InfrastructureException(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }
}
