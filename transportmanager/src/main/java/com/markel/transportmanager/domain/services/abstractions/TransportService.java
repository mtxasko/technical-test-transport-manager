package com.markel.transportmanager.domain.services.abstractions;

import com.markel.transportmanager.domain.exceptions.DomainException;
import com.markel.transportmanager.domain.model.Transport;

import java.util.List;
import java.util.Map;

public interface TransportService {
    List<Transport> getTransports() throws DomainException;

    void refreshTransports(String city, Map<String, String> filters) throws DomainException;
}
