package com.markel.transportmanager.domain.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.Map;

@Getter
@Setter
public class Transport {
    private String id;
    private String name;
    private String city;
    private Date lastUpdated;
    private Location location;
    private Boolean realTimeData;
    private String companyZoneId;
    private Map<String, Object> properties;

    @Override
    public boolean equals(Object obj) {
        if (!obj.getClass().equals(Transport.class)) {
            return false;
        }

        var t = (Transport) obj;
        return this.getId().equals(t.getId());
    }

    @Override
    public int hashCode() {
        return this.getId().hashCode();
    }
}
