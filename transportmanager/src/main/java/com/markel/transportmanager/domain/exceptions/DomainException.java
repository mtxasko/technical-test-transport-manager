package com.markel.transportmanager.domain.exceptions;

public class DomainException extends Exception {

    private final String message;

    public DomainException(InfrastructureException e) {
        this.message = e.getMessage();
    }

    public String getMessage() {
        return this.message;
    }
}
