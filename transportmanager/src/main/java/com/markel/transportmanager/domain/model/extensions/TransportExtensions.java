package com.markel.transportmanager.domain.model.extensions;

import com.markel.transportmanager.domain.model.Transport;

import java.util.Map;

public class TransportExtensions {

    public static boolean checkHasChanges(Transport currTransport, Transport newTransport) {
        return !currTransport.getId().equals(newTransport.getId()) ||
                !currTransport.getName().equals(newTransport.getName()) ||
                !currTransport.getCity().equals(newTransport.getCity()) ||
                !currTransport.getLocation().equals(newTransport.getLocation()) ||
                !currTransport.getCompanyZoneId().equals(newTransport.getCompanyZoneId()) ||
                checkPropertiesHaveChanges(currTransport.getProperties(), newTransport.getProperties());
    }

    private static boolean checkPropertiesHaveChanges(Map<String, Object> currProperties,
                                                      Map<String, Object> newProperties) {
        if (currProperties == null && newProperties == null) {
            return false;
        }

        return currProperties == null || !currProperties.equals(newProperties);
    }

}
