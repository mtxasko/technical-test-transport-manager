package com.markel.transportmanager.domain.ports;

import com.markel.transportmanager.domain.exceptions.InfrastructureException;
import com.markel.transportmanager.domain.model.Transport;

import java.util.List;
import java.util.Map;

public interface TransportProvider {
    List<Transport> getLatestTransports(String city, Map<String, String> params) throws InfrastructureException;
}
