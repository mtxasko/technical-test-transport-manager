package com.markel.transportmanager.domain.services;

import com.markel.transportmanager.domain.exceptions.DomainException;
import com.markel.transportmanager.domain.exceptions.InfrastructureException;
import com.markel.transportmanager.domain.model.Transport;
import com.markel.transportmanager.domain.model.extensions.TransportExtensions;
import com.markel.transportmanager.domain.ports.TransportNotifier;
import com.markel.transportmanager.domain.ports.TransportPersistence;
import com.markel.transportmanager.domain.ports.TransportProvider;
import com.markel.transportmanager.domain.services.abstractions.TransportService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class TransportServiceImpl implements TransportService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TransportServiceImpl.class);

    private final TransportProvider transportProvider;
    private final TransportNotifier transportNotifier;
    private final TransportPersistence transportPersistence;

    public TransportServiceImpl(TransportProvider transportProvider, TransportNotifier transportNotifier,
                                TransportPersistence transportPersistence) {
        this.transportProvider = transportProvider;
        this.transportNotifier = transportNotifier;
        this.transportPersistence = transportPersistence;
    }

    @Override
    public List<Transport> getTransports() throws DomainException {
        LOGGER.info("Retrieving transports...");
        try {
            return this.transportPersistence.getAll();
        } catch (InfrastructureException e) {
            throw new DomainException(e);
        }
    }

    @Override
    public void refreshTransports(String city, Map<String, String> filters) throws DomainException {
        LOGGER.info("Refreshing transports...");
        try {
            var existingTransports = this.transportPersistence.getAllByCity(city);
            var newTransports = this.transportProvider.getLatestTransports(city, filters);

            var createdUpdated = this.updateNewOrModifiedTransports(existingTransports, newTransports);
            this.transportNotifier.notifyCreatedUpdatedTransports(createdUpdated);

            var deleted = this.deleteRemovedTransports(existingTransports, newTransports);
            this.transportNotifier.notifyDeletedTransports(deleted);
        } catch (InfrastructureException e) {
            throw new DomainException(e);
        }
    }

    private List<Transport> updateNewOrModifiedTransports(List<Transport> existingTransports,
                                                          List<Transport> newTransports) throws InfrastructureException {
        var newOrModifiedTransports = this.filterNewOrModifiedTransports(existingTransports, newTransports);
        if (newOrModifiedTransports.isEmpty()) {
            return Collections.emptyList();
        }
        return this.transportPersistence.updateTransports(newOrModifiedTransports);
    }

    private List<Transport> filterNewOrModifiedTransports(List<Transport> existingTransports,
                                                          List<Transport> newTransports) {
        // If there are no stored transports, all the new ones have to be saved
        if (existingTransports.isEmpty()) {
            return newTransports;
        }

        var newOrModifiedTransports = new ArrayList<Transport>();

        newTransports.forEach(newTransport -> {
            var existingTransport = existingTransports.stream()
                    .filter(x -> x.equals(newTransport))
                    .findAny();
            if (existingTransport.isEmpty() ||
                    TransportExtensions.checkHasChanges(existingTransport.get(), newTransport)) {
                newOrModifiedTransports.add(newTransport);
            }
        });

        return newOrModifiedTransports;
    }

    private List<String> deleteRemovedTransports(List<Transport> existingTransports,
                                                 List<Transport> newTransports) throws InfrastructureException {
        // If there are no available transports, delete all the stored ones
        if (newTransports.isEmpty()) {
            return this.deleteTransports(existingTransports);
        }

        var transportsToDelete = existingTransports.stream()
                .filter(Predicate.not(newTransports::contains))
                .collect(Collectors.toList());

        return this.deleteTransports(transportsToDelete);
    }

    private List<String> deleteTransports(List<Transport> transportsToDelete) throws InfrastructureException {
        var transportsToDeleteIds = transportsToDelete.stream()
                .map(Transport::getId)
                .collect(Collectors.toList());
        this.transportPersistence.deleteTransportsById(transportsToDeleteIds);
        return transportsToDeleteIds;
    }
}
