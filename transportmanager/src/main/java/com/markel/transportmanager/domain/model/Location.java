package com.markel.transportmanager.domain.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Location {
    private String x;
    private String y;

    public Location(String x, String y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public boolean equals(Object obj) {
        if (getClass() != obj.getClass())
            return false;

        Location l = (Location) obj;
        return this.x.equals(l.getX()) &&
                this.y.equals(l.getY());
    }

    @Override
    public int hashCode() {
        return x.concat(y).hashCode();
    }
}
