# Prueba técnica

1. [ Instrucciones para lanzar el servicio ](#instrucciones)
2. [ Problemas encontrados ](#problemas)
3. [ Funcionalidades del servicio ](#funcionalidades)
4. [ Preguntas ](#preguntas)
    1. [ ¿Cómo de escalable es tu solución propuesta? ](#preguntas_1)
    2. [ ¿Qué problemas a futuro podría presentar? Si has detectado alguno, ¿Qué alternativa/s propones para solventar dichos problemas? ](#preguntas_2)
5. [ Librerías utilizadas ](#librerias)

<a name="instrucciones"></a>
## 1. Instrucciones para lanzar el servicio
1. Navegar a transportmanager/deployment
2. Si se desea, editar las variables del contenedor transport-manager en el *docker-compose.yml*
3. Ejecutar:
   ```
   docker compose up
   ```
4. Verificar que todo está funcionando correctamente a través de los logs

<a name="problemas"></a>
## 2. ***Problemas encontrados*
El ejercicio consistía en consumir de una API (https://apidev.meep.me/tripplan/api/v1/routers/lisboa/resources) con 
determinados parámetros. El viernes a última hora le eché un vistazo rápido y apunté un par de cosas, pero 
el sábado y domingo el endpoint no ha funcionado y no he podido hacer pruebas contra la API real:

![alt text](./readme_resources/apiError.PNG)

Para intentar solucionar este problemilla he creado un pequeño simulador que genera datos aleatorios (creo que se 
adecúan a las entidades reales que devuelve la API de Meep, pero no estoy 100% seguro) al que se puede llamar en 
vez de a la API real. Para activar o desactivar el simulador está la variable booleana *api.simulator* en el 
application.properties y en el docker-compose.

<a name="funcionalidades"></a>
## 3. Funcionalidades del servicio

Como el enunciado era bastante abierto, he pensado en este servicio como un encargado de gestionar los transportes 
de X compañías dentro de una zona geográfica concreta. Para esto, la aplicación se puede configurar con las 
siguientes variables de entorno:

- Ciudad (*api.city*).
- Parámetros a usar para llamar a la API de meep (*api.scheduler.params*) y así filtrar los transportes gestionados.
- Frecuencia de actualización (*api.scheduler.time*).

De esta forma, cuando pase el tiempo configurado, el servicio llamará al endpoint de Meep para la ciudad y los 
parámetros configurados y obtendrá los transportes. Una vez tenga los transportes disponibles, obtendrá un diferencial 
respecto a los que tiene guardados en base de datos (DynamoDB) y ejecutará las siguientes acciones:

- Guardar en BD los nuevos transportes.
- Modificar en BD los transportes cuyas propiedades han cambiado.
- Eliminar de BD (borrado físico) los transportes que ya no estén disponibles.
- Comunicar por un topic de Kafka los transportes añadidos/modificados y los eliminados.

De esta forma, los clientes que deseen contar con las últimas actualizaciones se podrían suscribir al topic de Kafka 
configurado y obtener los cambios en cada ejecución del scheduler. También, como pequeña prueba de concepto, he 
creado un endpoint /api/{city} que cuenta con los siguientes métodos ([colección de postman](./readme_resources/TransportManager.postman_collection.json)):

- GET -> Obtiene todos los datos de la BD (la entidad devuelta no es la del dominio, se han filtrado los campos a 
  devolver).
- POST -> Fuerza una actualización de los datos para no tener que esperar a que se ejecute el scheduler.

<a name="preguntas"></a>
## 4. Preguntas

<a name="preguntas_1"></a>
### 4.1. ¿Cómo de escalable es tu solución propuesta?
He intentado crear una arquitectura hexagonal lo más pura posible (en un único módulo de Java por simplicidad), 
con la capa de dominio totalmente agnóstica respecto al framework utilizado. Soy consciente de que este servicio no 
tiene muchas funcionalidades y que quizás la estructura utilizada pueda parecer un poco *overkill*, pero supongo que 
uno de los objetivos de esta prueba era valorar una arquitectura escalable y mantenible. Las 3 capas son las siguientes:

- Aplication
  - A parte de los controladores REST y el scheduler, he creado un paquete *applications* con servicios de aplicación.
Lo he hecho porque me parece lo más lógico en este tipo de arquitecturas, con el objetivo de que los encargados de 
orquestrar la lógica de negocio estén centralizados. De esta forma se evita que los controladores tengan cualquier tipo
de lógica y si en un futuro el desencadenante de un caso de uso viene a través de Kafka en vez de nuestra API REST,
únicamente tendríamos que llamar al servicio de aplicación.


- Domain
  - Aquí he creado el modelo, los servicios y las interfaces que usan las capas de aplicación (*services.abstractions*)
y de infraestructura (*ports*).

  - Para guardar los transportes he creado la entidad Transport, y al no poder acceder a los datos reales de la API de
  Meep y no tener datos de ejemplo, no he podido plantear ningún tipo de herencia para definir mejor los distintos tipos
  (coche, moto, tranvía, etc.), aunque si no se quieren realizar consultas complejas a BD quizás no sería ni necesario.
  Aún así, estas propiedades que no conocía (me suena que algunos vehículos tenían datos como nivel de batería, número
  de pasajeros, etc.) los he metido en un campo properties (un Map<String, Object>). Esto lo he hecho al deserializar el
  HttpContent de la respuesta, metiendo todas las propiedades no contempladas en este map. No he podido probarlo pero con
  mi simulador funciona bien, así que espero que con la API real también.


- Infrastructure
  - Un paquete por cada tipo de dependencia externa (*http* para las llamadas a la API de Meep, *persistence* para
  DynamoDB y *notifications* para Kafka). Las implementaciones de los puertos del dominio y las configuraciones
  específicas de estas dependencias (*DynamoDBConfig*, *HttpClientConfig* y *KafkaConfig/TopicsConfig*) las he metido
  en cada uno de estos paquetes.
  
  - Un paquete para la configuración de Spring que crea los beans del servicio de transporte (para no "ensuciar" el
  dominio con anotaciones de Spring como @Component) y el TransportProvider de la infraestructura, para crear el real
  que llama a la API de Meep o mi simulador, dependiendo de la configuración *api.simulator*.

<a name="preguntas_2"></a>
### 4.2. ¿Qué problemas a futuro podría presentar? Si has detectado alguno, ¿Qué alternativa/s propones para solventar dichos problemas?
- En primer lugar, la definición de la entidad Transporte sirve para todos los tipos y las particularidades de cada tipo
concreto se guardan en un campo properties, por lo que si en un futuro se quisieran hacer consultas con filtros basados
en estos campos, habría que darle una vuelta.


- No estoy 100% seguro de si la elección de DynamoDB es correcta o si Redis hubiera sido mejor opción. Para esta primera
aproximación Redis hubiera bastado pero, unido al punto anterior, no sé si podría ser una limitación en el futuro. Sí que
he visto que tanto DynamoDB como Redis tienen soporte para trabajar con datos geográficos (geohashes, filtrado en
persistencia a partir de límites geográficos, etc.), pero no me he metido mucho en detalle por falta de tiempo y no sé
cuál sería más apropiada en este caso. No obstante, creo que con la arquitectura propuesta el cambio de una tecnología a
otra sería sencillo, ya que únicamente habría que modificar el paquete *repositories*.


- Le daría una vuelta también a la gestión de excepciones. Es cierto que hace casi 2 años que no trabajo con Java/Spring
y no sé si mi implementación es correcta por los flujos seguidos. Supongo que cada equipo/proyecto suele implementar esto
de una forma distinta, pero aún así definiría unas reglas generales.


- No he hecho ningún test por falta de tiempo, así que habría que implementarlos.


- Por último, a nivel de estándar de código, he usado *var* (en .NET Core es muy común, al menos en los proyectos en los que he trabajado) y sé que
en Java no está tan estandarizado, así que no lo tengáis en cuenta si sois anti-var jeje (yo soy de la opinión de que
añade claridad al código y más con los IDEs modernos, que al estar todo tan tipado y siguiendo buenas prácticas de
nombrado, definir el tipo se hace innecesario). También, unido al punto anterior, no he usado Optional<> en los
contratos entre application/domain/infrastructure. Podría haber definido en los contratos del dominio devolver todo
encapsulado en Optional<> pero me he decantado por excepciones porque creo que se ajustan mejor a esos casos de uso.
Aún así, como lo de var, es simplemente preferencia personal y me puedo adaptar a lo que sea.

<a name="librerias"></a>
## 5. Librerías utilizadas
A parte de las necesarias para la BD, Kafka, etc. se han utilizado las siguientes librerías:

- Jease -> Para facilitar la creación de datos aleatorios del simulador
- Mapstruct -> Para el mapeo de entidades entre las distintas capas
- Lombok -> Para el boilerplate de getters/setters

